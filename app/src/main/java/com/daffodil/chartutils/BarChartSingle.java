package com.daffodil.chartutils;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class BarChartSingle extends LinearLayout {
    private BarChart lineChart;
    private float[] yValues;

    public BarChartSingle(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.activity_bar_chart_single, this, true);
        lineChart = (BarChart) getChildAt(0);
    }

    public void setData(float[] yValues){
        this.yValues = yValues;
        this.startPlottingChart();
    }

    private void startPlottingChart(){
        BarDataSet dataSet = new BarDataSet(getEntries(),"");
        BarData data = new BarData(getXLabels(),dataSet);
        data.setDrawValues(false);
        lineChart.setData(data);
        dataSet.setColor(Color.rgb(0xFB, 0xD8, 0x00));
        data.setDrawValues(false);
        dataSet.setBarSpacePercent(60f);
        ChartStyleHandler.custumozieBarChart(lineChart);
    }

    private ArrayList<BarEntry> getEntries(){
        ArrayList<BarEntry> entries = new ArrayList<>();
        for(int i=0;i<7;i++){
            entries.add(new BarEntry(yValues[i],i));
        }
        return entries;
    }

    public static ArrayList<String> getXLabels() {
        ArrayList<String> labels = new ArrayList<>();
        //labels.add("");
        labels.add("Studio");
        labels.add("1");
        labels.add("2");
        labels.add("3");
        labels.add("4");
        labels.add("5");
        labels.add("6+");
        return labels;
    }

}
