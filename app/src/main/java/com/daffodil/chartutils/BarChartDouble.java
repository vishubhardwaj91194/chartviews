package com.daffodil.chartutils;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class BarChartDouble extends LinearLayout {
    private BarChart lineChart;
    private float[] yValues1,yValues2;

    public BarChartDouble(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.activity_bar_chart_double, this, true);
        lineChart = (BarChart) getChildAt(0);
    }

    public void setData(float[] yValues1, float[] yValues2){
        this.yValues1 = yValues1;
        this.yValues2 = yValues2;
        this.startPlottingChart();
    }

    private void startPlottingChart(){
        BarData data = new BarData(getXLabels(),getEntries());
        data.setDrawValues(false);
        lineChart.setData(data);
        data.setDrawValues(false);
        ChartStyleHandler.custumozieBarChart(lineChart);
    }

    private ArrayList<BarDataSet> getEntries(){
        ArrayList<BarEntry> entries1 = new ArrayList<>();
        for(int i=0;i<6;i++){
            entries1.add(new BarEntry(yValues1[i],i));
        }
        ArrayList<BarEntry> entries2 = new ArrayList<>();
        for(int i=0;i<6;i++){
            entries2.add(new BarEntry(yValues2[i],i));
        }
        ArrayList<BarDataSet> dataSets = new ArrayList<>();
        BarDataSet dataSet1 = new BarDataSet(entries1,"");
        dataSet1.setColor(Color.rgb(0xFB, 0xCD, 0x00));

        BarDataSet dataSet2 = new BarDataSet(entries2,"");
        dataSet2.setColor(Color.rgb(0x00, 0xBB, 0xC5));

        dataSets.add(dataSet1);
        dataSets.add(dataSet2);
        return dataSets;
    }

    public static ArrayList<String> getXLabels() {
        ArrayList<String> labels = new ArrayList<>();
        //labels.add("");
        labels.add("Studio");
        labels.add("1");
        labels.add("2");
        labels.add("3");
        labels.add("4");
        labels.add("5");
        return labels;
    }
}
