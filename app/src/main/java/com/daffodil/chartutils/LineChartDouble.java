package com.daffodil.chartutils;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;

public class LineChartDouble extends LinearLayout {
    private LineChart lineChart;
    private float[] yValues1,yValues2;

    public LineChartDouble(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.activity_line_chart_double, this, true);
        this.lineChart = (LineChart) getChildAt(0);
    }

    public void setData(float[] yValues1, float[] yValues2){
        this.yValues1 = yValues1;
        this.yValues2 = yValues2;
        this.startPlottingChart();
    }

    private void startPlottingChart(){
        LineData data = new LineData(ChartStyleHandler.getXLabels(),getEntries());
        data.setDrawValues(false);
        lineChart.setData(data);
        ChartStyleHandler.custumozieLineChart(lineChart);
        lineChart.getXAxis().setAvoidFirstLastClipping(false);
    }

    private ArrayList<LineDataSet> getEntries(){
        ArrayList<Entry> entries1 = new ArrayList<>();
        for(int i=0;i<6;i++){
            entries1.add(new Entry(yValues1[i],i+1));
        }
        ArrayList<Entry> entries2 = new ArrayList<>();
        for(int i=0;i<6;i++){
            entries2.add(new Entry(yValues2[i],i+1));
        }
        ArrayList<LineDataSet> dataSets = new ArrayList<>();
        LineDataSet dataSet1 = new LineDataSet(entries1,"");
        dataSet1.setColor(Color.rgb(0xFB, 0xCD, 0x00));
        dataSet1.setCircleColor(Color.rgb(0xFB, 0xCD, 0x00));

        LineDataSet dataSet2 = new LineDataSet(entries2,"");
        dataSet2.setColor(Color.rgb(0x00, 0xBB, 0xC5));
        dataSet2.setCircleColor(Color.rgb(0x00, 0xBB, 0xC5));

        dataSets.add(dataSet1);
        dataSets.add(dataSet2);
        return dataSets;
    }
}
