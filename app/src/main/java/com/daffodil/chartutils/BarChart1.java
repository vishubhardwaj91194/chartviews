package com.daffodil.chartutils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

/**
 * A custom view created to generate a Bar chart with one or more data set.<br />
 * Library used : MPAndroidChart<br />
 * Author : Vishu Bhardwaj<br />
 * Usage :<br />
 * {@link BarChart1} barChart1 = ({@link BarChart1}) findViewById(R.id.chart32);<br />
 * ArrayList&lt;Float&gt; floatData3 = new ArrayList&lt;&gt;();<br />
 * floatData3.add(Float.valueOf(70000f));<br />
 * ...<br />
 * floatData3.add(Float.valueOf(270000f));<br />
 * ArrayList&lt;ArrayList&lt;Float&gt;&gt; data3 = new ArrayList&lt;&gt;();<br />
 * data3.add(floatData3);<br />
 * <br />
 * barChart1.{@link com.daffodil.chartutils.BarChart1#setData(ArrayList<ArrayList<Float>>) setData}(data3);<br />
 * ArrayList&lt;String&gt; label2 = new ArrayList&lt;&gt;();<br />
 * label2.add("Studio");<br />
 * ...<br />
 * label2.add("6+");<br />
 * <br />
 * barChart1.{@link com.daffodil.chartutils.BarChart1#setXValues(ArrayList<String>) setXValues}(label2);<br />
 * barChart1.{@link com.daffodil.chartutils.LineChart1#startPlotting() startPlotting}();<br />
 */
public class BarChart1 extends LinearLayout {
    private BarChart barChart;
    private ArrayList<ArrayList<Float>> data = new ArrayList<>();
    /*private int numSeries;
    private int numPoints;*/
    private ArrayList<String> xValues;
    private int numLines = ChartStyleHandler.numLines;
    private float min, max;

    /**
     * It is used to set the number of labels on Y axis and no of grid lines of Y axis<br />
     * uses int parameter lines by default it is 6
     *
     * @param lines
     */
    public void setNumLines(int lines) {
        this.numLines = lines;
    }

    /**
     * Constructor of custom view {@link BarChart1}.<br />
     * Uses layout inflator to get first child used as {@link BarChart} of MPAndroidChart
     *
     * @param context Activty context
     * @param attrs   i don't know what it is
     */
    public BarChart1(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.activity_bar_chart_single, this, true);
        barChart = (BarChart) getChildAt(0);
    }

    /**
     * It is used to set bar max levels to be drawn on {@link BarChart1}<br />
     *
     * @param data ArrayList of ArrayList of Float points
     */
    public void setData(ArrayList<ArrayList<Float>> data) {
        this.data = data;
        min = max = data.get(0).get(0).floatValue();
    }

    /**
     * It sets the X Axis labels for {@link BarChart}<br />
     *
     * @param xValues ArrayList of String
     */
    public void setXValues(ArrayList<String> xValues) {
        this.xValues = xValues;
    }

    private ArrayList<BarDataSet> getEntries() {
        ArrayList<ArrayList<BarEntry>> entrylist = new ArrayList<>(data.size());
        ArrayList<BarDataSet> dataSetList = new ArrayList<>();

        Float sample;
        float sample1;
        float numSeries = data.size();
        int i, j;
        ArrayList<Float> singleBarData;
        ArrayList<BarEntry> entry1;

        for (i = 0; i < numSeries; i++) {
            singleBarData = data.get(i);
            entry1 = new ArrayList<>();
            for (j = 0; j < singleBarData.size(); j++) {
                sample = singleBarData.get(j);
                // sample1 = Float.parseFloat(sample.floatValue());
                sample1 = sample.floatValue();
                if (sample1 > max) {
                    max = sample1;
                }
                if (sample1 < min) {
                    min = sample1;
                }
                entry1.add(new BarEntry(sample1, j));
            }
            entrylist.add(entry1);
        }

        BarDataSet dataSet;
        ArrayList<Integer> colors = ChartStyleHandler.getColors();

        for (i = 0; i < numSeries; i++) {
            dataSet = new BarDataSet(entrylist.get(i), "");
            dataSet.setColor(colors.get(i).intValue());
            if (numSeries == 1) {
                dataSet.setBarSpacePercent(60f);
            }/* else if (i == 0) {
                dataSet.setBarSpacePercent(50f);
            } else if (i == 1) {
                dataSet.setBarSpacePercent(50f);
            }*/
            /*else{
                dataSet.setBarSpacePercent(40f);
            }*/

            dataSetList.add(dataSet);
        }

        return dataSetList;
    }

    /**
     * It starts the computation for plotting chart.<br />
     * It is called after all values have been set<br />
     * Following methods need to be called before it<br />
     * {@link com.daffodil.chartutils.BarChart1#setData(ArrayList<ArrayList<Float>>) setData()}<br />
     * {@link com.daffodil.chartutils.BarChart1#setXValues(ArrayList<String>) setXValues()}<br />
     */
    public void startPlotting() {
        BarData bardata = new BarData(xValues, getEntries());
        bardata.getGroupSpace();
        bardata.setDrawValues(false);
        barChart.setData(bardata);
        if (data.size() != 1) {
            bardata.setGroupSpace(200f);
        }

        float diff = max - min;
        float lineDiff = diff / (numLines - 2);
//        float minline = (int)(min/lineDiff)*lineDiff - lineDiff;
        float maxline = (int) (max / lineDiff) * lineDiff + lineDiff;
        /*
            Old
            ChartStyleHandler.custumozieBarChart1(barChart, ((int) (min / 10000)) * 10000, ((int) (max / 10000) + 10) * 10000);
            New
        */
        ChartStyleHandler.custumozieBarChart1(barChart, 0, maxline);
        barChart.getAxisLeft().setLabelCount(numLines, true);
        barChart.getXAxis().setAvoidFirstLastClipping(false);
        //barChart.highlightValue(2,0);
    }

    /**
     * It is used to highlight a bar in {@link BarChart}<br />
     * Heighlighting involves darker shade of bar which is heighlighted<br />
     * <br />
     * @param xIndex the index of bar in a series. 0 meaning first bar.
     * @param dataSetIndex the series index in all data set i.e bar set. 0 meaning first series of bars or first dataset.
     */
    public void highlightValue(int xIndex, int dataSetIndex){
        barChart.highlightValue(xIndex, dataSetIndex);
    }

}
