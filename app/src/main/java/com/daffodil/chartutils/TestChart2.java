package com.daffodil.chartutils;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

public class TestChart2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_chart2);

        LineChart1 lineChart1 = (LineChart1) findViewById(R.id.chart12);

        ArrayList<Float> floatData1 = new ArrayList<>();
        floatData1.add(Float.valueOf(175000f));
        floatData1.add(Float.valueOf(185000f));
        floatData1.add(Float.valueOf(195000f));
        floatData1.add(Float.valueOf(185000f));
        floatData1.add(Float.valueOf(155000f));
        floatData1.add(Float.valueOf(175000f));
        ArrayList<ArrayList<Float>> data1 = new ArrayList<>();
        data1.add(floatData1);

        lineChart1.setData(data1);

        ArrayList<String> label1 = new ArrayList<>();
        label1.add("Feb");
        label1.add("Mar");
        label1.add("Apr");
        label1.add("May");
        label1.add("Jun");
        label1.add("Jul");

        lineChart1.setXValues(label1);
        lineChart1.startPlotting();

        LineChart1 lineChart2 = (LineChart1) findViewById(R.id.chart22);
        ArrayList<Float> floatData2 = new ArrayList<>();
        floatData2.add(Float.valueOf(178000f));
        floatData2.add(Float.valueOf(200000f));
        floatData2.add(Float.valueOf(173000f));
        floatData2.add(Float.valueOf(162000f));
        floatData2.add(Float.valueOf(173000f));
        floatData2.add(Float.valueOf(182000f));
        ArrayList<ArrayList<Float>> data2 = new ArrayList<>();
        data2.add(floatData1);
        data2.add(floatData2);
        lineChart2.setData(data2);

        lineChart2.setXValues(label1);
        lineChart2.startPlotting();

        BarChart1 barChart1 = (BarChart1) findViewById(R.id.chart32);
        ArrayList<Float> floatData3 = new ArrayList<>();
        floatData3.add(Float.valueOf(70000f));
        floatData3.add(Float.valueOf(140000f));
        floatData3.add(Float.valueOf(170000f));
        floatData3.add(Float.valueOf(400000f));
        floatData3.add(Float.valueOf(230000f));
        floatData3.add(Float.valueOf(250000f));
        floatData3.add(Float.valueOf(270000f));
        ArrayList<ArrayList<Float>> data3 = new ArrayList<>();
        data3.add(floatData3);

        barChart1.setData(data3);
        ArrayList<String> label2 = new ArrayList<>();
        label2.add("Studio");
        label2.add("1");
        label2.add("2");
        label2.add("3");
        label2.add("4");
        label2.add("5");
        label2.add("6+");

        barChart1.setXValues(label2);
        barChart1.startPlotting();

        BarChart1 barChart2 = (BarChart1) findViewById(R.id.chart42);
        ArrayList<Float> floatData5 = new ArrayList<>();
        floatData5.add(Float.valueOf(70000f));
        floatData5.add(Float.valueOf(140000f));
        floatData5.add(Float.valueOf(170000f));
        floatData5.add(Float.valueOf(400000f));
        floatData5.add(Float.valueOf(230000f));
        floatData5.add(Float.valueOf(250000f));

        ArrayList<Float> floatData4 = new ArrayList<>();
        floatData4.add(Float.valueOf(90000f));
        floatData4.add(Float.valueOf(160000f));
        floatData4.add(Float.valueOf(200000f));
        floatData4.add(Float.valueOf(360000f));
        floatData4.add(Float.valueOf(280000f));
        floatData4.add(Float.valueOf(270000f));
        ArrayList<ArrayList<Float>> data4 = new ArrayList<>();
        data4.add(floatData5);
        data4.add(floatData4);

        barChart2.setData(data4);
        ArrayList<String> label3 = new ArrayList<>();
        label3.add("Studio");
        label3.add("1");
        label3.add("2");
        label3.add("3");
        label3.add("4");
        label3.add("5");


        barChart2.setXValues(label3);
        barChart2.startPlotting();

        /*lineChart1.highlightValue(2,0);
        barChart1.highlightValue(2,0);*/
    }
}
