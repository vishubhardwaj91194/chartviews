package com.daffodil.chartutils;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class Chart3 extends AppCompatActivity {

    private BarChart chart3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart3);

        chart3 = (BarChart) findViewById(R.id.chart3);
        BarData barData = new BarData(getXLabels(), getDataSet());
        barData.setDrawValues(false);
        chart3.setData(barData);
        ChartStyleHandler.custumozieBarChart(chart3);

    }

    private ArrayList<BarDataSet> getDataSet(){
        ArrayList<BarEntry> entry1 = new ArrayList<>();
        entry1.add(new BarEntry(70000f, 0));
        entry1.add(new BarEntry(140000f, 1));
        entry1.add(new BarEntry(170000f, 2));
        entry1.add(new BarEntry(400000f, 3));
        entry1.add(new BarEntry(230000f, 4));
        entry1.add(new BarEntry(260000f, 5));

        ArrayList<BarEntry> entry2 = new ArrayList<>();
        entry2.add(new BarEntry(90000f, 0));
        entry2.add(new BarEntry(160000f, 1));
        entry2.add(new BarEntry(200000f, 2));
        entry2.add(new BarEntry(360000f, 3));
        entry2.add(new BarEntry(280000f, 4));
        entry2.add(new BarEntry(270000f, 5));

        BarDataSet barDataSet1 = new BarDataSet(entry1, "");
        barDataSet1.setColor(Color.rgb(0xFB, 0xCD, 0x00));
        //barDataSet1.setBarSpacePercent(60f);

        BarDataSet barDataSet2 = new BarDataSet(entry2,"");
        barDataSet2.setColor(Color.rgb(0x00,0xBB,0xC5));
        //barDataSet1.setBarSpacePercent(60f);

        ArrayList<BarDataSet> arayBDS = new ArrayList<>();
        arayBDS.add(barDataSet1);
        arayBDS.add(barDataSet2);
        return arayBDS;
    }

    public static ArrayList<String> getXLabels() {
        ArrayList<String> labels = new ArrayList<>();
        //labels.add("");
        labels.add("Studio");
        labels.add("1");
        labels.add("2");
        labels.add("3");
        labels.add("4");
        labels.add("5");
        return labels;
    }
}
