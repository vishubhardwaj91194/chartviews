package com.daffodil.chartutils;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;

public class LineChartSingle extends LinearLayout {
    private LineChart lineChart;
    private float[] yValues;

    public LineChartSingle(Context context, AttributeSet attr) {
        super(context, attr);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.activity_line_chart_single, this, true);
        lineChart = (LineChart) getChildAt(0);
    }

    public void setData(float[] yValues) {
        this.yValues = yValues;
        this.startPlottingChart();
    }

    private void startPlottingChart() {
        LineDataSet dataSet = new LineDataSet(getEntries(), "");
        LineData data = new LineData(ChartStyleHandler.getXLabels(), dataSet);
        data.setDrawValues(false);
        lineChart.setData(data);
        dataSet.setColor(Color.rgb(0xFB, 0xD8, 0x00));
        dataSet.setCircleColor(Color.rgb(0xFB, 0xD8, 0x00));
        ChartStyleHandler.custumozieLineChart(lineChart);
        lineChart.getXAxis().setAvoidFirstLastClipping(false);
    }

    private ArrayList<Entry> getEntries() {
        ArrayList<Entry> entries = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            entries.add(new Entry(yValues[i], i + 1));
        }
        return entries;
    }

}
