package com.daffodil.chartutils;

import android.graphics.Color;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;

import java.util.ArrayList;

public class ChartStyleHandler {

    public static void custumozieBarChart(BarChart lineChart) {
        lineChart.getAxisLeft().setAxisMinValue(0);
        lineChart.getAxisLeft().setAxisMaxValue(500000);
        lineChart.getAxisLeft().setStartAtZero(true);
        lineChart.getAxisRight().setEnabled(false);
        lineChart.getAxisLeft().setDrawAxisLine(false);
        lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        lineChart.getXAxis().setDrawGridLines(false);


        lineChart.setGridBackgroundColor(Color.WHITE);
        lineChart.getAxisLeft().setValueFormatter(new YAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, YAxis yAxis) {
                int val = (int) value;
                String x = String.valueOf(val);
                if (val > 1000) {
                    val = val / 1000;
                    x = String.valueOf(val) + "k";
                } else if (val == 0) {
                    x = String.valueOf(val) + "k";
                }
                return x;
            }
        });

        lineChart.getXAxis().setGridColor(Color.rgb(0x82, 0x82, 0x82));
        lineChart.getXAxis().setAxisLineColor(Color.rgb(0x82, 0x82, 0x82));
        lineChart.getXAxis().setTextColor(Color.rgb(0x82, 0x82, 0x82));

        //lineChart.getAxisLeft().setGridColor(Color.rgb(0x82, 0x82, 0x82));
        //lineChart.getAxisLeft().setAxisLineColor(Color.rgb(0x82, 0x82, 0x82));
        lineChart.getAxisLeft().setTextColor(Color.rgb(0x82, 0x82, 0x82));

        lineChart.setPinchZoom(false);
        lineChart.setDescription("");
        lineChart.getLegend().setEnabled(false);
        lineChart.getAxisLeft().setYOffset(-5);

        lineChart.getXAxis().setGridLineWidth(1);
        lineChart.getXAxis().setAxisLineWidth(1);
        lineChart.dispatchSetSelected(false);
        lineChart.setSelected(false);

        lineChart.setDoubleTapToZoomEnabled(false);
        lineChart.setTouchEnabled(false);

    }

    public static ArrayList<String> getXLabels() {
        ArrayList<String> labels = new ArrayList<>();
        labels.add("");
        labels.add("Feb");
        labels.add("Mar");
        labels.add("Apr");
        labels.add("May");
        labels.add("Jun");
        labels.add("Jul");
        labels.add("");
        return labels;
    }

    public static void custumozieLineChart(LineChart lineChart) {
        lineChart.getAxisLeft().setAxisMinValue(150000);
        lineChart.getAxisLeft().setAxisMaxValue(200000);
        lineChart.getAxisLeft().setStartAtZero(false);
//        lineChart.getAxisRight().setEnabled(false);
        lineChart.getAxisLeft().setDrawAxisLine(false);
        lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        lineChart.getXAxis().setDrawGridLines(false);


        lineChart.setGridBackgroundColor(Color.WHITE);
        lineChart.getAxisLeft().setValueFormatter(new YAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, YAxis yAxis) {
                int val = (int) value;
                String x = String.valueOf(val);
                if (val > 1000) {
                    val = val / 1000;
                    x = String.valueOf(val) + "k";
                }
                return x;
            }
        });

        lineChart.getXAxis().setGridColor(Color.rgb(0x82, 0x82, 0x82));
        lineChart.getXAxis().setAxisLineColor(Color.rgb(0x82, 0x82, 0x82));
        lineChart.getXAxis().setTextColor(Color.rgb(0x82, 0x82, 0x82));

        //lineChart.getAxisLeft().setGridColor(Color.rgb(0x82,0x82,0x82));
        //lineChart.getAxisLeft().setAxisLineColor(Color.rgb(0x82,0x82,0x82));
        lineChart.getAxisLeft().setTextColor(Color.rgb(0x82, 0x82, 0x82));

        lineChart.setPinchZoom(false);
        lineChart.setDescription("");
        lineChart.getLegend().setEnabled(false);
        lineChart.getAxisLeft().setYOffset(-5);

        //lineChart.getXAxis().setAvoidFirstLastClipping(true);
        //lineChart.getXAxis().setXOffset(-10);
        lineChart.getXAxis().setSpaceBetweenLabels(1);


        lineChart.getXAxis().setGridLineWidth(1);
        lineChart.getXAxis().setAxisLineWidth(1);
        lineChart.dispatchSetSelected(false);
        lineChart.setSelected(false);

        lineChart.setDoubleTapToZoomEnabled(false);
        lineChart.setTouchEnabled(false);


        lineChart.getAxisRight().setDrawLabels(true);
        lineChart.getAxisRight().setDrawAxisLine(true);
        lineChart.getAxisRight().setTextColor(Color.TRANSPARENT);
        lineChart.getAxisRight().setAxisLineColor(Color.TRANSPARENT);
        lineChart.getAxisRight().setXOffset(0);
        lineChart.getAxisLeft().setXOffset(10);
        lineChart.getAxisRight().setDrawGridLines(false);
    }

    public static ArrayList<Integer> getColors() {
        ArrayList<Integer> colors = new ArrayList<>();
        int color = Color.rgb(0xFB, 0xD8, 0x00);
        colors.add(Integer.valueOf(color));
        color = Color.rgb(0x00, 0xBB, 0xC5);
        colors.add(Integer.valueOf(color));
        return colors;
    }

    public static ArrayList<String> getXValues(ArrayList<String> xVal) {
        ArrayList<String> labels = new ArrayList<>();
        labels.add("");
        for (int i = 0; i < xVal.size(); i++) {
            labels.add(xVal.get(i));
        }
        labels.add("");
        return labels;
    }

    public static void custumozieLineChart1(LineChart lineChart, float min, float max) {
        lineChart.getAxisLeft().setAxisMinValue(min);
        lineChart.getAxisLeft().setAxisMaxValue(max);
        lineChart.getAxisLeft().setStartAtZero(false);
//        lineChart.getAxisRight().setEnabled(false);
        lineChart.getAxisLeft().setDrawAxisLine(false);
        lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        lineChart.getXAxis().setDrawGridLines(false);


        lineChart.setGridBackgroundColor(Color.TRANSPARENT);
        lineChart.getAxisLeft().setValueFormatter(new YAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, YAxis yAxis) {
                int val = (int) value;
                String x = String.valueOf(val);
                if (val > 1000) {
                    val = val / 1000;
                    x = String.valueOf(val) + "k";
                }
                return x;
            }
        });

        lineChart.getXAxis().setGridColor(Color.rgb(0x82, 0x82, 0x82));
        lineChart.getXAxis().setAxisLineColor(Color.rgb(0x82, 0x82, 0x82));
        lineChart.getXAxis().setTextColor(Color.rgb(0x82, 0x82, 0x82));

        //lineChart.getAxisLeft().setGridColor(Color.rgb(0x82,0x82,0x82));
        //lineChart.getAxisLeft().setAxisLineColor(Color.rgb(0x82,0x82,0x82));
        lineChart.getAxisLeft().setTextColor(Color.rgb(0x82, 0x82, 0x82));

        lineChart.setPinchZoom(false);
        lineChart.setDescription("");
        lineChart.getLegend().setEnabled(false);
        lineChart.getAxisLeft().setYOffset(-5);

        //lineChart.getXAxis().setAvoidFirstLastClipping(true);
        //lineChart.getXAxis().setXOffset(-10);
        lineChart.getXAxis().setSpaceBetweenLabels(1);


        lineChart.getXAxis().setGridLineWidth(1);
        lineChart.getXAxis().setAxisLineWidth(1);
        lineChart.dispatchSetSelected(false);
        lineChart.setSelected(false);

        lineChart.setDoubleTapToZoomEnabled(false);
        lineChart.setTouchEnabled(false);


        lineChart.getAxisRight().setEnabled(false);
        lineChart.getAxisLeft().setXOffset(10);
    }

    public static void custumozieBarChart1(BarChart lineChart, float min, float max) {
        lineChart.getAxisLeft().setAxisMinValue(0);
        lineChart.getAxisLeft().setAxisMaxValue(max);
        lineChart.getAxisLeft().setStartAtZero(true);
        lineChart.getAxisRight().setEnabled(false);
        lineChart.getAxisLeft().setDrawAxisLine(false);
        lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        lineChart.getXAxis().setDrawGridLines(false);


        lineChart.setGridBackgroundColor(Color.TRANSPARENT);
        lineChart.getAxisLeft().setValueFormatter(new YAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, YAxis yAxis) {
                int val = (int) value;
                String x = String.valueOf(val);
                if (val > 1000) {
                    val = val / 1000;
                    x = String.valueOf(val) + "k";
                } else if (val == 0) {
                    x = String.valueOf(val) + "k";
                }
                return x;
            }
        });

        lineChart.getXAxis().setGridColor(Color.rgb(0x82, 0x82, 0x82));
        lineChart.getXAxis().setAxisLineColor(Color.rgb(0x82, 0x82, 0x82));
        lineChart.getXAxis().setTextColor(Color.rgb(0x82, 0x82, 0x82));

        //lineChart.getAxisLeft().setGridColor(Color.rgb(0x82, 0x82, 0x82));
        //lineChart.getAxisLeft().setAxisLineColor(Color.rgb(0x82, 0x82, 0x82));
        lineChart.getAxisLeft().setTextColor(Color.rgb(0x82, 0x82, 0x82));

        lineChart.setPinchZoom(false);
        lineChart.setDescription("");
        lineChart.getLegend().setEnabled(false);
        lineChart.getAxisLeft().setYOffset(-5);

        lineChart.getXAxis().setGridLineWidth(1);
        lineChart.getXAxis().setAxisLineWidth(1);
        lineChart.dispatchSetSelected(false);
        lineChart.setSelected(false);

        lineChart.setDoubleTapToZoomEnabled(false);
        lineChart.setTouchEnabled(false);

    }

    public static int numLines = 6;
}
