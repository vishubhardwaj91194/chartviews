package com.daffodil.chartutils;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class TestChart1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_chart1);
        //LineChartSingle chartSingle = new LineChartSingle(getApplicationContext());
        LineChartSingle chartSingle = (LineChartSingle) findViewById(R.id.chart12);
        float[] data = {175000f,185000f,195000f,185000f,155000f,175000f};
        chartSingle.setData(data);

        LineChartDouble chartSingle4 = (LineChartDouble) findViewById(R.id.chart42);
        float[] data1 = {178000f,200000f,173000f,162000f,173000f,182000f};
        chartSingle4.setData(data,data1);

        BarChartSingle chartSingle2 = (BarChartSingle) findViewById(R.id.chart22);
        float[] data21 = {70000f,140000f,170000f,400000f,230000f,250000f,270000f,};
        chartSingle2.setData(data21);

        BarChartDouble chartSingle3 = (BarChartDouble) findViewById(R.id.chart32);
        float[] data22  = {90000f,160000f,200000f,360000f,280000f,270000f};
        chartSingle3.setData(data21,data22);
    }
}
