package com.daffodil.chartutils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;

/**
 * A custom view created to generate a line chart with one or more data set.<br />
 * Library used : MPAndroidChart<br />
 * Author : Vishu Bhardwaj<br />
 * Usage :<br />
 * {@link LineChart1} lineChart1 = ({@link LineChart1}) findViewById(R.id.chart12);<br />
 * ArrayList&lt;Float&gt; floatData1 = new ArrayList&lt;&gt;();<br />
 * floatData1.add(Float.valueOf(175000f));<br />
 * ...<br />
 * floatData1.add(Float.valueOf(175000f));<br />
 * ArrayList&lt;ArrayList&lt;Float&gt;&gt; data1 = new ArrayList&lt;&gt;();<br />
 * data1.add(floatData1);<br />
 * <br />
 * lineChart1.{@link com.daffodil.chartutils.LineChart1#setData(ArrayList<ArrayList<Float>>) setData}(data1);<br />
 * <br />
 * ArrayList&lt;String&gt; label1 = new ArrayList&lt;&gt;();<br />
 * label1.add("Feb");<br />
 * ...<br />
 * label1.add("Jul");<br />
 * <br />
 * lineChart1.{@link com.daffodil.chartutils.LineChart1#setXValues(ArrayList<String>) setXValues}(label1);<br />
 * lineChart1.{@link com.daffodil.chartutils.LineChart1#startPlotting() startPlotting}();<br />
 */
public class LineChart1 extends LinearLayout {
    private LineChart lineChart;
    private ArrayList<ArrayList<Float>> data = new ArrayList<>();
    /*private int numSeries;
    private int numPoints;*/
    private ArrayList<String> xValues;
    private int numLines = ChartStyleHandler.numLines;
    private float min, max;

    /**
     * It is used to set the number of labels on Y axis and no of grid lines of Y axis<br />
     * uses int parameter lines by default it is 6
     *
     * @param lines
     */
    public void setNumLines(int lines) {
        this.numLines = lines;
    }

    /**
     * Constructor of custom view {@link LineChart1}.<br />
     * Uses layout inflator to get first child used as {@link LineChart} of MPAndroidChart
     *
     * @param context Activty context
     * @param attrs   i don't know what it is
     */
    public LineChart1(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.activity_line_chart, this, true);
        lineChart = (LineChart) getChildAt(0);
    }

    /**
     * It is used to set the points to be plotted on {@link LineChart}<br />
     *
     * @param data ArrayList of ArrayList of Float points
     */
    public void setData(ArrayList<ArrayList<Float>> data) {
        this.data = data;
        min = max = data.get(0).get(0).floatValue();
    }

    /**
     * It sets the X Axis labels for {@link LineChart}<br />
     *
     * @param xValues ArrayList of String
     */
    public void setXValues(ArrayList<String> xValues) {
        this.xValues = xValues;
    }

    private ArrayList<LineDataSet> getEntries() {
        ArrayList<ArrayList<Entry>> entrylist = new ArrayList<>();
        ArrayList<LineDataSet> dataSetList = new ArrayList<>();

        Float sample;
        float sample1;
        float numSeries = data.size();
        int i, j;
        ArrayList<Float> singleLineData;
        ArrayList<Entry> entry1;

        for (i = 0; i < numSeries; i++) {
            singleLineData = data.get(i);
            entry1 = new ArrayList<>();
            for (j = 0; j < singleLineData.size(); j++) {
                sample = singleLineData.get(j);
                // sample1 = Float.parseFloat(sample.floatValue());
                sample1 = sample.floatValue();
                if (sample1 > max) {
                    max = sample1;
                }
                if (sample1 < min) {
                    min = sample1;
                }
                entry1.add(new Entry(sample1, j+1));
            }
            entrylist.add(entry1);
        }

        LineDataSet dataSet;
        ArrayList<Integer> colors = ChartStyleHandler.getColors();

        for (i = 0; i < numSeries; i++) {
            dataSet = new LineDataSet(entrylist.get(i), "");
            dataSet.setColor(colors.get(i).intValue());
            dataSet.setCircleColor(colors.get(i).intValue());
            dataSetList.add(dataSet);
        }

        return dataSetList;
    }

    /**
     * It starts the computation for plotting chart.<br />
     * It is called after all values have been set<br />
     * Following methods need to be called before it<br />
     * {@link com.daffodil.chartutils.LineChart1#setData(ArrayList<ArrayList<Float>>) setData()}<br />
     * {@link com.daffodil.chartutils.LineChart1#setXValues(ArrayList<String>) setXValues()}<br />
     */
    public void startPlotting() {
        LineData lineData = new LineData(ChartStyleHandler.getXValues(xValues), getEntries());
        lineData.setDrawValues(false);
        lineChart.setData(lineData);

        float diff = max - min;
        float lineDiff = diff / (numLines - 2);
        // float minline = (int)(min/lineDiff)*lineDiff - lineDiff;
        float minline = ((min) + (lineDiff - ((int) min % (int) lineDiff))) - lineDiff;
        float maxline = (int) (max / lineDiff) * lineDiff + lineDiff;

        /*
            Old
            ChartStyleHandler.custumozieLineChart1(lineChart,((int)(min/10000))*10000,((int)(max/10000)+1)*10000);
            New
        */
        ChartStyleHandler.custumozieLineChart1(lineChart, minline, maxline);

        lineChart.getAxisLeft().setLabelCount(numLines, true);
        lineChart.getXAxis().setAvoidFirstLastClipping(false);
        //lineChart.highlightValue(3,0);

//        lineChart.setViewPortOffsets(40f, 0f, 0f, 0f);
//        lineChart.setExtraLeftOffset(40f);

    }

    /**
     * It is used to highlight a point in {@link LineChart}<br />
     * Heighlighting involves drawing X and Y grid lines to that point<br />
     *
     * @param xIndex the index of point in a series. 1 meaning first point. always start with 1 not 0.
     * @param dataSetIndex the series index in all data set i.e line set. 0 meaning first series of line or first dataset.
     */
    public void highlightValue(int xIndex, int dataSetIndex){
        lineChart.highlightValue(xIndex, dataSetIndex);
    }

}
