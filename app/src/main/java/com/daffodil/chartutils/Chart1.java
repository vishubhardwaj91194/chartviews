package com.daffodil.chartutils;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;

public class Chart1 extends AppCompatActivity {

    private LineChart lineChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart1);

        lineChart = (LineChart) findViewById(R.id.chart1);
        final LineDataSet dataSet = new LineDataSet(getEntries(), "");

        final LineData data = new LineData(ChartStyleHandler.getXLabels(), dataSet);
        data.setDrawValues(false);

        lineChart.setData(data);


        dataSet.setColor(Color.rgb(0xFB, 0xD8, 0x00));
        dataSet.setCircleColor(Color.rgb(0xFB, 0xD8, 0x00));
        ChartStyleHandler.custumozieLineChart(lineChart);

/*
        lineChart.setHighlightPerTapEnabled(false);
        lineChart.setHighlightPerDragEnabled(false);

        dataSet.setDrawHighlightIndicators(false);
        dataSet.setDrawHorizontalHighlightIndicator(false);
        dataSet.setHighlightEnabled(false);
        data.setHighlightEnabled(false);
*/
/*
        lineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                lineChart.highlightValue(null);
            }

            @Override
            public void onNothingSelected() {

            }
        });*/

        lineChart.getXAxis().setAvoidFirstLastClipping(false);


    }

    private ArrayList<Entry> getEntries() {
        ArrayList<Entry> entries = new ArrayList<>();
        entries.add(new Entry(175000f, 1));
        entries.add(new Entry(185000f, 2));
        entries.add(new Entry(195000f, 3));
        entries.add(new Entry(185000f, 4));
        entries.add(new Entry(155000f, 5));
        entries.add(new Entry(175000f, 6));
//        entries.add(new Entry(,));
        return entries;
    }

}
