package com.daffodil.chartutils;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class Chart2 extends AppCompatActivity {

    private BarChart chart2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart2);

        chart2 = (BarChart) findViewById(R.id.chart2);
        BarDataSet dataSet = new BarDataSet(getEntries(), "");
        ArrayList<BarDataSet> data1 = new ArrayList<>();
        data1.add(dataSet);
        BarData data = new BarData(getXLabels(), data1);
        chart2.setData(data);

        dataSet.setColor(Color.rgb(0xFB, 0xCD, 0x00));
        data.setDrawValues(false);
        dataSet.setBarSpacePercent(60f);
        ChartStyleHandler.custumozieBarChart(chart2);
    }

    private ArrayList<BarEntry> getEntries() {
        ArrayList<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(70000f, 0));
        entries.add(new BarEntry(140000f, 1));
        entries.add(new BarEntry(170000f, 2));
        entries.add(new BarEntry(400000f, 3));
        entries.add(new BarEntry(230000f, 4));
        entries.add(new BarEntry(250000f, 5));
        entries.add(new BarEntry(270000f, 6));
//        entries.add(new Entry(,));
        return entries;
    }

    public static ArrayList<String> getXLabels() {
        ArrayList<String> labels = new ArrayList<>();
        //labels.add("");
        labels.add("Studio");
        labels.add("1");
        labels.add("2");
        labels.add("3");
        labels.add("4");
        labels.add("5");
        labels.add("6+");
        return labels;
    }


}
