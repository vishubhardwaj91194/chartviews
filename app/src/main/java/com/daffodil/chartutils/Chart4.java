package com.daffodil.chartutils;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;

public class Chart4 extends AppCompatActivity {

    private LineChart lineChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart4);

        lineChart = (LineChart) findViewById(R.id.chart4);
        LineData data = new LineData(ChartStyleHandler.getXLabels(), getEntrires());
        data.setDrawValues(false);
        lineChart.setData(data);
        ChartStyleHandler.custumozieLineChart(lineChart);

        lineChart.getXAxis().setAvoidFirstLastClipping(false);


    }

    private ArrayList<LineDataSet> getEntrires() {
        ArrayList<Entry> entry1 = new ArrayList<>();
        entry1.add(new Entry(175000f, 1));
        entry1.add(new Entry(185000f, 2));
        entry1.add(new Entry(195000f, 3));
        entry1.add(new Entry(185000f, 4));
        entry1.add(new Entry(155000f, 5));
        entry1.add(new Entry(175000f, 6));

        ArrayList<Entry> entry2 = new ArrayList<>();
        entry2.add(new Entry(178000f, 1));
        entry2.add(new Entry(200000f, 2));
        entry2.add(new Entry(173000f, 3));
        entry2.add(new Entry(162000f, 4));
        entry2.add(new Entry(173000f, 5));
        entry2.add(new Entry(182000f, 6));

        LineDataSet dataSet1 = new LineDataSet(entry1, "");
        dataSet1.setColor(Color.rgb(0xFB, 0xCD, 0x00));
        dataSet1.setCircleColor(Color.rgb(0xFB, 0xCD, 0x00));

        LineDataSet dataSet2 = new LineDataSet(entry2, "");
        dataSet2.setColor(Color.rgb(0x00, 0xBB, 0xC5));
        dataSet2.setCircleColor(Color.rgb(0x00, 0xBB, 0xC5));

        ArrayList<LineDataSet> linedata = new ArrayList<>();
        linedata.add(dataSet1);
        linedata.add(dataSet2);

        return linedata;
    }
}
