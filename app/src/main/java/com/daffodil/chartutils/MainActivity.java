package com.daffodil.chartutils;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b1 = (Button) findViewById(R.id.button);
        b1.setOnClickListener(this);

        Button b2 = (Button) findViewById(R.id.button2);
        b2.setOnClickListener(this);

        Button b3 = (Button) findViewById(R.id.button3);
        b3.setOnClickListener(this);

        Button b4 = (Button) findViewById(R.id.button4);
        b4.setOnClickListener(this);

        Button b5 = (Button) findViewById(R.id.button5);
        b5.setOnClickListener(this);

        Button b7 = (Button) findViewById(R.id.button7);
        b7.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i = null;
        if (v.getId() == R.id.button) {
            i = new Intent(this, Chart1.class);
        } else if (v.getId() == R.id.button2) {
            i = new Intent(this, Chart2.class);
        } else if (v.getId() == R.id.button3) {
            i = new Intent(this, Chart3.class);
        } else if (v.getId() == R.id.button4) {
            i = new Intent(this, Chart4.class);
        } else if(v.getId() == R.id.button5) {
            i = new Intent(this, TestChart1.class);
        } else if(v.getId() == R.id.button7){
            i = new Intent(this, TestChart2.class);
        }
        startActivity(i);
    }
}
