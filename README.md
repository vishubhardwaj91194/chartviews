# README #

# Line Chart #

![chart1.png](https://bitbucket.org/repo/pLoKGk/images/1546627409-chart1.png)

## Layout XML ##

```
#!xml

<com.daffodil.chartutils.LineChart1
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:id="@+id/chart12"
                />
```

### Single Line Chart ###

```
#!java

LineChart1 lineChart1 = (LineChart1) findViewById(R.id.chart12);

        ArrayList<Float> floatData1 = new ArrayList<>();
        floatData1.add(Float.valueOf(175000f));
        floatData1.add(Float.valueOf(185000f));
        floatData1.add(Float.valueOf(195000f));
        floatData1.add(Float.valueOf(185000f));
        floatData1.add(Float.valueOf(155000f));
        floatData1.add(Float.valueOf(175000f));
        ArrayList<ArrayList<Float>> data1 = new ArrayList<>();
        data1.add(floatData1);

        lineChart1.setData(data1);

        ArrayList<String> label1 = new ArrayList<>();
        label1.add("Feb");
        label1.add("Mar");
        label1.add("Apr");
        label1.add("May");
        label1.add("Jun");
        label1.add("Jul");

        lineChart1.setXValues(label1);
        lineChart1.startPlotting();
```

### Double Line Chart ###

```
#!java

LineChart1 lineChart2 = (LineChart1) findViewById(R.id.chart22);
        ArrayList<Float> floatData1 = new ArrayList<>();
        floatData1.add(Float.valueOf(175000f));
        floatData1.add(Float.valueOf(185000f));
        floatData1.add(Float.valueOf(195000f));
        floatData1.add(Float.valueOf(185000f));
        floatData1.add(Float.valueOf(155000f));
        floatData1.add(Float.valueOf(175000f));

        ArrayList<Float> floatData2 = new ArrayList<>();
        floatData2.add(Float.valueOf(178000f));
        floatData2.add(Float.valueOf(200000f));
        floatData2.add(Float.valueOf(173000f));
        floatData2.add(Float.valueOf(162000f));
        floatData2.add(Float.valueOf(173000f));
        floatData2.add(Float.valueOf(182000f));
        ArrayList<ArrayList<Float>> data2 = new ArrayList<>();
        data2.add(floatData1);
        data2.add(floatData2);
        lineChart2.setData(data2);

        lineChart2.setXValues(label1);
        lineChart2.startPlotting();
```

# Bar Chart #

![chart2.png](https://bitbucket.org/repo/pLoKGk/images/712405008-chart2.png)

## Layout XML ##

```
#!xml

<com.daffodil.chartutils.BarChart1
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:id="@+id/chart32"
                />
```

### Single Bar Chart ###


```
#!java

BarChart1 barChart1 = (BarChart1) findViewById(R.id.chart32);
        ArrayList<Float> floatData3 = new ArrayList<>();
        floatData3.add(Float.valueOf(70000f));
        floatData3.add(Float.valueOf(140000f));
        floatData3.add(Float.valueOf(170000f));
        floatData3.add(Float.valueOf(400000f));
        floatData3.add(Float.valueOf(230000f));
        floatData3.add(Float.valueOf(250000f));
        floatData3.add(Float.valueOf(270000f));
        ArrayList<ArrayList<Float>> data3 = new ArrayList<>();
        data3.add(floatData3);

        barChart1.setData(data3);
        ArrayList<String> label2 = new ArrayList<>();
        label2.add("Studio");
        label2.add("1");
        label2.add("2");
        label2.add("3");
        label2.add("4");
        label2.add("5");
        label2.add("6+");

        barChart1.setXValues(label2);
        barChart1.startPlotting();
```

### Double Bar Chart ###


```
#!java

BarChart1 barChart2 = (BarChart1) findViewById(R.id.chart42);
        ArrayList<Float> floatData5 = new ArrayList<>();
        floatData5.add(Float.valueOf(70000f));
        floatData5.add(Float.valueOf(140000f));
        floatData5.add(Float.valueOf(170000f));
        floatData5.add(Float.valueOf(400000f));
        floatData5.add(Float.valueOf(230000f));
        floatData5.add(Float.valueOf(250000f));

        ArrayList<Float> floatData4 = new ArrayList<>();
        floatData4.add(Float.valueOf(90000f));
        floatData4.add(Float.valueOf(160000f));
        floatData4.add(Float.valueOf(200000f));
        floatData4.add(Float.valueOf(360000f));
        floatData4.add(Float.valueOf(280000f));
        floatData4.add(Float.valueOf(270000f));
        ArrayList<ArrayList<Float>> data4 = new ArrayList<>();
        data4.add(floatData5);
        data4.add(floatData4);

        barChart2.setData(data4);
        ArrayList<String> label3 = new ArrayList<>();
        label3.add("Studio");
        label3.add("1");
        label3.add("2");
        label3.add("3");
        label3.add("4");
        label3.add("5");


        barChart2.setXValues(label3);
        barChart2.startPlotting();
```


### What is this repository for? ###

* Quick summary
It is a chart library which can be used to generate either Line chart or Bar chart. It has a very simple process. 2 custom views are made to create Line chart
* Version 1.2
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
Include this repository in your project and create a view in your xml of name LineChart1 or BarChart1 and use it to generate chart by given methods
* Dependencies
Depends on MPAndroidChart

### Who do I talk to? ###

* Vishu Bhardwaj <vishu.bhardwaj@daffodilsw.com>